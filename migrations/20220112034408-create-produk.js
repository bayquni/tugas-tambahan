'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Produks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nama: {
        allowNull: false,
        type: Sequelize.STRING
      },
      kategori: {
        allowNull: false,
        type: Sequelize.STRING
      },
      stok: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      harga: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      warna: {
        allowNull: false,
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      sellerId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references : {
          model :  "Sellers",
          target : "id"
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Produks');
  }
};