const express = require('express');
const router = express.Router();
const RoutesSeller = require('./routes_seller');
const RoutesProduk = require('./routes_produk');
const { authorize, authenticate }= require('../middlewares/auth');

router.use('/seller', RoutesSeller);
router.use('/produk', authenticate, RoutesProduk);

module.exports = router;