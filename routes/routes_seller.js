const express = require('express');
const router = express.Router();
const ContSeller = require('../controllers/cont_seller')

router.post('/register', ContSeller.postRegister);
router.post('/Login', ContSeller.postLogin);
router.post('/list', ContSeller.getSeller);

module.exports = router;