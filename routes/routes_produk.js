const express = require('express');
const router = express.Router();
const ContProduk = require('../controllers/cont_produk')
const { authenticate, authorize }  = require('../middlewares/auth')

router.post('/post', authenticate, ContProduk.postProduk);
router.get('/get', authenticate, ContProduk.getProduk);
router.delete('/delete/:id', authenticate, authorize, ContProduk.deleteProduk);
router.put('/update/:id', authenticate, authorize, ContProduk.putProduk);

module.exports = router;