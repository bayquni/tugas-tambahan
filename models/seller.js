'use strict';
const { hashPassword } = require('../helpers/bcrypt')
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Seller extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Seller.hasMany(models.Produk, {foreignKey: "sellerId"})
    }
  };
  Seller.init({
    email:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "E-mail Harus Diisi"
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [4,8],
          msg: `Harus 4 -8 Karakter`
        }
      }
    },
    no_telepon:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Telepon Harus Diisi"
        }
      }
    },
    no_rek:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Nomor Rekening Harus Diisi"
        }
      }
    }
  }, {
    sequelize,
    modelName: 'Seller',
    hooks: {
      beforeCreate: (seller) => {
        seller.password = hashPassword(seller.password); //sebelum dimasukan kje dnb dihash dulu
      }
    }
  });
  return Seller;
};