'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Produk extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // Produk.belongsTo(models.Seller, {foreignkey:"sellerId"})
      Produk.belongsTo(models.Seller)
      // Produk.belongsTo(models.Seller, {target:"email"})
    }
  };
  Produk.init({
    nama:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Nama Harus Diisi"
        }
      }
    },
    kategori:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Kategori Harus Diisi"
        }
      }
    },
    stok:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Stok Harus Diisi"
        }
      }
    },
    harga:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Harga Harus Diisi"
        }
      }
    },
    warna:  {
      type: DataTypes.STRING,
      validate: {
        notEmpty: {
          args: true,
          msg: "Warna Harus Diisi"
        }
      }
    },
    sellerId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Produk',
  });
  return Produk;
};