const { Produk, Seller } = require('../models')

class ContProduk {
    static postProduk(req, res) {
        // console.log(req.body, req.seller.id)
        const { nama, kategori, stok, harga, warna,  } = req.body;

        Produk.create({
            nama, 
            kategori, 
            stok, 
            harga, 
            warna, 
            sellerId: req.seller.id,
        })

        .then ((data) => {
            console.log(data)
            res.status(201).json({msg: `Produk Sukses Diinput`, data})
        })

        .catch((err) => {
            console.log(err)
        })
    }

    static getProduk(req, res) {
        const sellerId = req.seller.id
        console.log(sellerId)
        Produk.findAll({
                        // nama: Seller.nama,
            attributes: ["nama", "kategori", "stok", "harga", "warna"],
            where: {
                sellerId: sellerId
            },
            include: {
                model: Seller,
                attributes: ["email", "no_telepon"]
            }
        })

        .then((data) => {
            if (!data) {
                res.status(500).json({msg :`Belum Ada Data`})
            }
            else {
                res.status(200).json({
                    msg: data
                })
            }
        })

        .catch((err) => {
            console.log(err)
            res.status(201).json({msg: `Error Mengambil Data`})
        })
    }

    static putProduk(req, res) {
        const id = +req.params.id;
        const { nama, kategori, stok, harga, warna, ukuran  } = req.body;
        Produk.update({ nama, kategori, stok, harga, warna, ukuran },
        { where: {id}, returning: true 
    
        })

        .then((data) => {
            res.status(200).json({msg: `Sukses Edit Produk`})
        })
        
        .catch((err) => {
            console.log(req.body)
            res.status(500).json({msg: `Error Edit Produk`})
        })

    }

    static deleteProduk(req, res) {
        const id = +req.params.id;

        Produk.destroy({
            where: {id}
        })
        
        .then((data) => {
            if (!data) {
            res.status(404).json({msg: `Produk Tidak Ditemukan`})
            }
            else {
            res.status(200).json({msg: `Produk Terhapus`})
            }
        })

        .catch((err) => {
            console.log(err);
            res.status(500).json({msg: `Error Hapus Produk`})
        })

    }
}

module.exports = ContProduk;