const { Seller } = require('../models');
const { comparePassword } = require('../helpers/bcrypt');
const { generateToken } = require('../helpers/jwt')

class ContSeller {
    static postRegister(req, res) {
        const { email, password, no_telepon, no_rek } = req.body;

        Seller.create({
            email, 
            password, 
            no_telepon, 
            no_rek
        })

        .then((sellerData) => {
            console.log(sellerData)
            let sellerShow = {
                email: sellerData.email,
                no_telepon: sellerData.no_telepon,
                no_rekening: sellerData.no_rekening
            }
            res.status(201).json({msg: 'Berhasil Mendaftar', sellerShow})
        })

        .catch((err) => {
            console.log(err);
            res.status(500).json({
                msg: `Error `,
                "System Info": err.message //menampilkan error dalam json

            })
        })

    }

    static postLogin (req, res) {
        const { email, password } = req.body;
        Seller.findOne({
            where: {
                email: email
            }
        })

        .then((dataLogin) => {
            if (dataLogin) {
                let checkPassword = comparePassword(password, dataLogin.password);
                if (checkPassword) {
                    let payload = {
                        id: dataLogin.id,
                        email: dataLogin.email
                    }

                    const access_token = generateToken(payload);
                    res.status(200).json({access_token});
                }
                else {
                    res.status(401).json({msg: `Email/Pasword`})
                }

            } 
            else {
                res.status(401).json({msg: `Email/Pasword`})
            }
        })

        .catch((err) => {res.status(500).json({msg: `Error Login`})})
    }

    static getSeller(req, res) {
        Produk.findAll()

        .then((data) => {
            if (!data) {
                res.status(500).json({msg :`Belum Ada Data`})
            }
            else {
                res.status(200).json({
                    msg: data
                })
            }
        })

        .catch((err) => {
            console.log(err)
            res.status(201).json({msg: `Error Mengambil Data`})
        })
    }

}


module.exports = ContSeller;