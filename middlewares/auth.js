const { Produk, Seller } = require('../models');
const { verifyToken } = require('../helpers/jwt');


function authenticate(req,res,next) {
    const { access_token } = req.headers;
    if (access_token) {
        const decoded = verifyToken(access_token);
        Seller.findOne({
            where: {
                email: decoded.email
            }
        })

        .then((seller) => {
            if (!seller) {
                res.status(401).json({msg: `Tidak Ada Otorisasi`});
            } 
            else {
                req.seller = {id : seller.id};
                next();
            }
        })

        .catch((err) => {
            res.status(500).json({msg: `Error nih`})
        })
    }
    else {
        res.status(401).json({msg: `Tidak Ada Akses`})
    }
}

function authorize(req,res,next) {
    const id = +req.params.id;

    Produk.findOne({where: {id}})
    
    .then((data_authorize) => {
        if (data_authorize) {
            const valid = req.seller.id === data_authorize.sellerId;
            if (valid) {
                next();
            }
            else {
                res.status(401).json(`Belum Ada Data`)
            }
        }
        else {
            res.status(401).json(`TIdak Ada Otorisasi`)
        }
    })

    .catch((err) => {
        console.log(err);
        res.status(500).json({msg: `Error Otorisasi`})
    })
}


module.exports = {
    authenticate,
    authorize
}