const jwt = require('jsonwebtoken');

function generateToken(payload) {
    const token = jwt.sign(payload, process.env.SALT);
    return token;
}

function verifyToken(token) {
    const verify = jwt.verify(token, process.env.SALT);
    return verify;
}

module.exports = {
    generateToken,
    verifyToken
}